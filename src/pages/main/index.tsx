import SideBar from "@/components/Sidebar";
import React from "react";

const Main = () => {
  return (
    <>
      <SideBar />
    </>
  );
};

export default Main;

import { Header } from "@/components/common/Header";
import styled from "@emotion/styled";
import React from "react";
import { Outlet } from "react-router-dom";

const MainLayout = () => {
  return (
    <>
      <Header />
      <StyledLayout>
        <Outlet />
      </StyledLayout>
    </>
  );
};

const StyledLayout = styled.div`
  padding: 60px 44px;
`;

export default MainLayout;

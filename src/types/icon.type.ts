import * as Icons from "@/assets/icons";

export type IconNameType = keyof typeof Icons;

import React from "react";
import { FolderBtn, ItemSection, SidebarWrapper, Text } from "./style";
import { Icon } from "../common/Icon";
import Button from "../common/Button";
import Divider from "../common/Divider";

const SideBar = () => {
  return (
    <SidebarWrapper>
      <ItemSection>
        <FolderBtn to="/home">
          <Icon name="Data" /> 내 데이터
        </FolderBtn>
        <FolderBtn to="/folders/shared">
          <Icon name="People" /> 다른 사람이 공유한 항목
        </FolderBtn>
      </ItemSection>
      <Divider />
      <ItemSection>
        <Button icon={<Icon name="Upload" />} text="업로드" mode="full" />
        <Button icon={<Icon name="NewFolder" />} text="새 폴더" />
      </ItemSection>
      <Divider />
      <ItemSection align="center">
        <Text>데스크톱에서 데스크톱 커넥터로 파일 동기화 및 작업</Text>
        <Icon name="CloudSync" />
        <Button text="데스크탑 커넥터 가져오기" />
      </ItemSection>
      <Divider />
      <ItemSection>
        <Text>프로젝트 워크플로우를 한 단계 높여 보십시오.</Text>
        <Button icon={<Icon name="Fusion360" />} text="Fusion 360" />
      </ItemSection>
    </SidebarWrapper>
  );
};

export default SideBar;

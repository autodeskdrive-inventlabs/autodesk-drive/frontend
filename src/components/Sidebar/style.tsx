import styled from "@emotion/styled";
import { NavLink } from "react-router-dom";

export const SidebarWrapper = styled.section`
  display: flex;
  width: 210px;
  flex-direction: column;
  padding: 1.5rem 0.75rem;
  gap: 20px;
`;

export const ItemSection = styled.div<{ align?: "start" | "center" | "end" }>`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: ${(props) => (props.align ? props.align : "start")};
  gap: 15px;

  .active {
    color: #0696d7;
    font-weight: bold;
  }
`;

export const FolderBtn = styled(NavLink)`
  display: flex;
  width: 190px;
  overflow-x: hidden;
  align-items: center;
  gap: 10px;
  padding: 10px;
  border-radius: 4px;
  font-size: 0.875rem;

  &:hover {
    background-color: #f9f9f9;
  }
`;

export const Text = styled.span`
  font-size: 0.75rem;
  color: #4a4a4a;
`;

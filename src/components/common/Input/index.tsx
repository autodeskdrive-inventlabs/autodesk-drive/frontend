import styled from "@emotion/styled";
import { InputHTMLAttributes } from "react";

const Input = (props: InputHTMLAttributes<HTMLInputElement>) => {
  return <StyledInput {...props} />;
};

const StyledInput = styled.input`
  border: 1px solid #dbdbdb;
  border-radius: 2px;
  padding: calc(0.6em - 1px) calc(0.75em - 1px);

  &::placeholder {
    font-size: 0.75rem;
  }
`;

export default Input;

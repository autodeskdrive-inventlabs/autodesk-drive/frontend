import styled from "@emotion/styled";
import React, { ButtonHTMLAttributes } from "react";

interface BtnProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  icon?: React.JSX.Element;
  text: string;
  mode?: "full" | "stroke";
}

const Button = ({ mode = "stroke", text, icon, ...props }: BtnProps) => {
  return (
    <StyledBtn mode={mode} {...props}>
      {icon}
      {text}
    </StyledBtn>
  );
};

const StyledBtn = styled.button<{ mode: "full" | "stroke" }>`
  display: flex;
  width: 100%;
  gap: 7px;
  text-align: left;
  align-items: center;
  padding: 10px;
  border: 1px solid #0696d7;
  border-radius: 4px;
  font-weight: bolder;
  background-color: ${(props) => (props.mode === "full" ? "#0696d7" : "white")};
  color: ${(props) => (props.mode === "full" ? "white" : "#0696d7")};
  cursor: pointer;

  &:hover {
    box-shadow: 0 0 0 0.25em rgba(6, 150, 215, 0.25);
  }
`;

export default Button;

import styled from "@emotion/styled";

const Divider = styled.div`
  width: 100%;
  height: 1px;
  background-color: #c2c2c2;
`;

export default Divider;

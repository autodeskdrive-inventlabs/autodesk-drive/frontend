import { HTMLAttributes, createElement } from "react";
import { IconNameType } from "@/types/icon.type";
import getIconFromName from "@/utils/getIcon";

interface IconProps extends HTMLAttributes<HTMLDivElement> {
  name?: IconNameType;
  color?: string;
}

export const Icon = ({ name, color, ...props }: IconProps) => {
  return (
    <>{createElement(getIconFromName(name), { style: { fill: color } })}</>
  );
};

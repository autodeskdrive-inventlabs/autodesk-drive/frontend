import React from "react";
import { HeaderWrapper, RightSide } from "./style";
import Profile from "./Profile";
import Help from "./Help";
import { Icon } from "../Icon";
import Input from "../Input";

export const Header = () => {
  return (
    <HeaderWrapper>
      <Icon name="Logo" />
      <RightSide>
        <Input placeholder="파일 검색" />
        <Help />
        <Profile />
      </RightSide>
    </HeaderWrapper>
  );
};

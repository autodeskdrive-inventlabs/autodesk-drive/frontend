import styled from "@emotion/styled";

export const HeaderWrapper = styled.header`
  display: flex;
  position: fixed;
  width: 100%;
  z-index: 2;
  padding: 0 32px;
  align-items: center;
  justify-content: space-between;
  min-height: 60px;
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.1);
`;

export const RightSide = styled.div`
  display: flex;
  gap: 12px;
`;

import styled from "@emotion/styled";
import React from "react";
import { Icon } from "@/components/common/Icon";

const Help = () => {
  return (
    <HelpWrapper>
      <Icon name="Help" />
      <span>도움말</span>
    </HelpWrapper>
  );
};

const HelpWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
  font-size: 12px;
`;

export default Help;

import styled from "@emotion/styled";
import React from "react";

const Profile = () => {
  return (
    <StyledProfile src="https://yt3.ggpht.com/yti/AGOGRCrFqyLYiQG5zX-yEqMlqmqEOi6HxcfysQsXAQ=s108-c-k-c0x00ffffff-no-rj" />
  );
};

const StyledProfile = styled.img`
  border-radius: 100%;
  width: 32px;
  height: 32px;
`;

export default Profile;

import { ReactComponent as Help } from "./help.svg";
import { ReactComponent as Logo } from "./logo.svg";
import { ReactComponent as Data } from "./data.svg";
import { ReactComponent as People } from "./people.svg";
import { ReactComponent as Upload } from "./upload.svg";
import { ReactComponent as NewFolder } from "./new_folder.svg";

const Fusion360 = () => (
  <img
    width={20}
    src="https://drive.autodesk.com/v2.21.2/images/fusion-360.png"
  />
);

const CloudSync = () => (
  <img
    width={80}
    src="https://drive.autodesk.com/v2.21.2/images/cloud-sync.png"
  />
);

export { Help, Logo, Data, People, Upload, NewFolder, Fusion360, CloudSync };

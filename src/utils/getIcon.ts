import * as Icons from "@/assets/icons";
import { IconNameType } from "@/types/icon.type";

export const getIconFromName = (name: IconNameType = "Help") => {
  return Icons[name];
};

export default getIconFromName;

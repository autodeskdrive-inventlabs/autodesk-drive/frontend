import { CommonRouter } from "@/components/router/CommonRouter";

const App = () => (
  <>
    <CommonRouter />
  </>
);

export default App;
